﻿using lutece_fdm;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lutece_FDM_Windows
{
    public partial class Form1 : Form
    {
        public Task<string> UpdateTask { get; set; }

        public Version Version { get; set; }

        public Form1()
        {
            InitializeComponent();

            var frFr = new CultureInfo("fr-FR");
            CultureInfo.CurrentUICulture = frFr;
            CultureInfo.CurrentCulture = frFr;

            var thisApp = Assembly.GetExecutingAssembly();
            AssemblyName name = new AssemblyName(thisApp.FullName);
            Version = name.Version;

            Text += $" (v.{Version})";

            UpdateTask = Task.Run(() => CheckUpdate());
        }

        private async Task<string> CheckUpdate()
        {
            HttpClient req = new HttpClient();
            var result = await req.GetAsync("http://www.vekn.fr/bloodbowl/lutece_fdm_update.txt");
            if (result.IsSuccessStatusCode)
            {
                var content = await result.Content.ReadAsStringAsync();
                var index = content.IndexOf(':');
                var version = content.Substring(0, index);
                var url = content.Substring(index + 1);

                if (Version.ToString() != version)
                {
                    return url;
                }
            }
            return "";
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            var url = textBox1.Text;
            if (!UrlValidator.IsUrlValid(url))
            {
                if (MessageBox.Show(
                    "L'url a l'air suspecte, elle devrait ressembler à : https://www.lutececup.org/roster.php?match_id=...&team_id=...\nContinuer quand même ?",
                    "Url suspecte",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Warning) == DialogResult.Cancel)
                {
                    return;
                }
            }

            string htmlCode;
            using (WebClient client = new WebClient())
            {
                try
                {
                    richTextBox1.AppendText($"Téléchargement en cours de {url}.\n");
                    var htmlData = client.DownloadData(url);
                    htmlCode = Encoding.UTF8.GetString(htmlData);
                    richTextBox1.AppendText($"Téléchargement réussi.\n");
                }
                catch (Exception ex)
                {
                    var text = "Impossible de télécharger la feuille de match.\n" + ex.ToString() + "\n";
                    //MessageBox.Show(text, "Erreur lors du téléchargement", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    richTextBox1.AppendText(text);
                    return;
                }
            }

            var teamParser = new TeamParser();
            List<Team> teams;

            try
            {
                teams = teamParser.CollectTeams(htmlCode);
            }
            catch (Exception ex)
            {
                var text = "Erreur d'analyse de la feuille de match.\n" + ex.ToString() + "\n";
                //MessageBox.Show(text, "Erreur lors de l'analyse", MessageBoxButtons.OK, MessageBoxIcon.Error);
                richTextBox1.AppendText(text);
                return;
            }


            string outputPath = ReplaceInvalidChars($"Lutece - {teams[0].TeamName.Val} vs {teams[1].TeamName.Val}.pdf");

            saveFileDialog1.FileName = outputPath;
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }

            outputPath = saveFileDialog1.FileName;
            richTextBox1.AppendText($"Génération de la feuille de match vers {outputPath}\n");
            Application.DoEvents();

            var generator = new PdfGenerator();

            try
            {
                generator.Create(teams, outputPath);
                richTextBox1.AppendText($"Génération réussie.\n");
            }
            catch (Exception ex)
            {
                var text = "Erreur de génération du PDF.\n" + ex.ToString() + "\n";
                //MessageBox.Show(text, "Erreur lors de l'analyse", MessageBoxButtons.OK, MessageBoxIcon.Error);
                richTextBox1.AppendText(text);
                return;
            }

            string ReplaceInvalidChars(string filename) => string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (UpdateTask.IsCompleted)
            {
                timer1.Enabled = false;
                string url = UpdateTask.Result;
                if (!string.IsNullOrEmpty(url))

                    if (MessageBox.Show("Une nouvelle version est disponible, souhaitez-vous la télécharger ?", "Nouvelle version disponible", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        try
                        {
                            using (var webClient = new WebClient())
                            {
                                var knownFolderGuid = new Guid("{374DE290-123F-4565-9164-39C4925E467B}"); // Downloads

                                var filepath = UpdateTask.Result.Substring(UpdateTask.Result.LastIndexOf('/') + 1);
                                filepath = Path.Combine(GetPath(knownFolderGuid, KnownFolderFlags.DontVerify, false), filepath);
                                webClient.DownloadFile(new Uri(url), filepath);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show($"Erreur lors du téléchargement de {url}.\r\n" + ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        Process.Start("shell:Downloads");
                    }
            }
        }

        private static string GetPath(Guid knownFolderGuid, KnownFolderFlags flags, bool defaultUser)
        {
            int result = SHGetKnownFolderPath(knownFolderGuid,
                (uint)flags, new IntPtr(defaultUser ? -1 : 0), out IntPtr outPath);
            if (result >= 0)
            {
                string path = Marshal.PtrToStringUni(outPath);
                Marshal.FreeCoTaskMem(outPath);
                return path;
            }
            else
            {
                throw new ExternalException("Unable to retrieve the known folder path. It may not "
                    + "be available on this system.", result);
            }
        }

        [DllImport("Shell32.dll")]
        private static extern int SHGetKnownFolderPath([MarshalAs(UnmanagedType.LPStruct)] Guid rfid, uint dwFlags, IntPtr hToken, out IntPtr ppszPath);

        [Flags]
        private enum KnownFolderFlags : uint
        {
            SimpleIDList = 0x00000100,
            NotParentRelative = 0x00000200,
            DefaultPath = 0x00000400,
            Init = 0x00000800,
            NoAlias = 0x00001000,
            DontUnexpand = 0x00002000,
            DontVerify = 0x00004000,
            Create = 0x00008000,
            NoAppcontainerRedirection = 0x00010000,
            AliasOnly = 0x80000000
        }
    }
}
