﻿using System.Diagnostics;

namespace lutece_fdm
{
    public enum Modifier
    {
        None,
        Bonus,
        Malus,
    }

    [DebuggerDisplay("{Val,nq}")]
    public struct Value<T>(T value, bool isBold = false, Modifier modifier = Modifier.None)
    {
        public bool IsBold { get; set; } = isBold;
        public Modifier Modifier { get; set; } = modifier;
        public T Val { get; set; } = value;
    }
}
