﻿using iTextSharp.text;
using iTextSharp.text.pdf;

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace lutece_fdm
{
    public class PdfGenerator
    {
        public static BaseColor LIGHTER_GRAY = new(220, 220, 220);
        public static BaseColor BOLD_RED = new(0x8B, 0x00, 0x00);
        public static BaseColor BOLD_GREEN = new(0x00, 0x80, 0x00);

        public Font BigFont { get; }
        public Font UnicodeFont { get; }
        public Font RegularFont { get; }
        public Font SmallFont { get; }
        public Font SmallerFont { get; }
        public Font BoldFont { get; }
        public Font TeamFont { get; }
        public Font RegularFontRed { get; }
        public Font RegularFontGray { get; }
        public Font BoldFontWhite { get; }
        public Font BoldFontGray { get; }
        public Font EmojiFont { get; }
        public Font EmojiFontWhite { get; }

        static PdfGenerator()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        public PdfGenerator()
        {
            BigFont = Get("arial", 14);
            UnicodeFont = Get("arialuni", 8);
            RegularFont = Get("arial", 8);
            SmallFont = Get("arial", 7);
            SmallerFont = Get("arial", 6);
            BoldFont = Get("arial", 8, null, Font.BOLD);
            TeamFont = Get("impact", 14);
            RegularFontRed = Get("arialuni", 8, BaseColor.RED);
            RegularFontGray = Get("arialuni", 8, LIGHTER_GRAY);
            BoldFontWhite = Get("arial", 8, BaseColor.WHITE, Font.BOLD);
            BoldFontGray = Get("arial", 8, LIGHTER_GRAY, Font.BOLD);
            EmojiFont = Get("NotoEmoji-Regular", 8);
            EmojiFontWhite = Get("NotoEmoji-Regular", 8, BaseColor.WHITE);
        }

        public void Create(List<Team> teams, string outputPath)
        {
            var doc = new Document(new RectangleReadOnly(842f, 595f));
            doc.SetMargins(24, 24, 24, 24);

            var writer = PdfWriter.GetInstance(doc, new FileStream(outputPath, FileMode.Create));
            writer.PageEvent = new CustomChunkTagEventHelper();

            doc.Open();

            var firstPage = true;
            foreach (var team in teams)
            {
                if (!firstPage)
                    doc.NewPage();
                firstPage = false;
                WriteTeam(doc, team, team.TeamValue.Val - teams.First(t => t != team).TeamValue.Val);
            }

            doc.Close();
        }

        public Font Get(string fontName, float fontSize, BaseColor color = null, int style = 0)
        {
            var file = $"{fontName}.ttf";
            var path = $@"Resources\{file}";
            if (!File.Exists(path))
            {
                path = Path.Combine(Environment.GetEnvironmentVariable("SystemRoot"), "fonts", file);
                if (!File.Exists(path))
                {
                    throw new Exception("Cannot find font file " + fontName + ".ttf");
                }
            }

            return new Font(BaseFont.CreateFont(path, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), fontSize, style, color ?? BaseColor.BLACK);
        }

        private Phrase ValueToPhrase<T>(Value<T> value, Font font = null)
        {
            return CreatePhrase(ValueToChunk(value, font));
        }

        private Chunk ValueToChunk<T>(Value<T> value, Font font = null)
        {
            var text = value.Val?.ToString();
            font ??= value.Modifier == Modifier.None
                ? (value.IsBold ? BoldFont : RegularFont)
                : BoldFontWhite;
            var chunk = new Chunk(text) { Font = font };
            if (value.IsBold && value.Modifier == Modifier.None) chunk.SetGenericTag(CustomChunkTagEventHelper.CIRCLE);
            return chunk;
        }

        private PdfPCell CreateCell(Phrase phrase)
        {
            return new PdfPCell(phrase)
            {
                VerticalAlignment = Element.ALIGN_TOP,
                Border = Rectangle.NO_BORDER,
            };
        }

        private Chunk CreateChunk(string text, Font font = null)
        {
            return new Chunk(text) { Font = font ?? RegularFont };
        }

        private Phrase CreatePhrase(string text, Font font = null)
        {
            return CreatePhrase(CreateChunk(text, font));
        }

        private Phrase CreatePhrase(params Chunk[] chunks)
        {
            var phrase = new Phrase() { MultipliedLeading = 0.5f };
            foreach (var chunk in chunks.Where(c => c != null))
            {
                phrase.Add(chunk);
            }
            return phrase;
        }

        private void WriteTeam(Document doc, Team team, int diff)
        {
            PdfPTable table = new(3);
            table.AddCell(CreateCell(new Phrase(team.CoachName.Val)).ToLeft());
            table.AddCell(CreateCell(new Phrase(CreateChunk($"{team.TeamName.Val} ({team.TeamValue.Val / 10000m})", TeamFont))).ToCenter());
            table.AddCell(CreateCell(new Phrase(team.Race.Val)).ToRight());
            doc.Add(table);

            const string TWO_DIGITS = "00";
            const string FOUR_DIGITS = "00/00";
            const string SIX_DIGITS = "00/00/00";

            var maxNameLength = team.Players.Max(p => p.Name.Val.Length);
            var nameFont = maxNameLength < 40 ? RegularFont : (maxNameLength < 60 ? SmallFont : SmallerFont);
            string lastPosition = null;

            var dataCells = new Func<Player, ColumnInfo>[] {
                //(Player p) => ("N°", ValueToPhrase(p.Number), c => c.HorizontalAlignment = Element.ALIGN_CENTER, TWO_DIGITS),
                (Player p) => new("Nom", ValueToPhrase(p.Name, nameFont)),
                (Player p) => new("Poste", ValueToPhrase(p.Position)),
                (Player p) => new("M", ValueToPhrase(p.MA), (c, _) => c.HorizontalAlignment = Element.ALIGN_CENTER, TWO_DIGITS, p.MA.Modifier),
                (Player p) => new("F", ValueToPhrase(p.ST), (c, _) => c.HorizontalAlignment = Element.ALIGN_CENTER, TWO_DIGITS, p.ST.Modifier),
                (Player p) => new("AG", ValueToPhrase(p.AG), (c, _) => c.HorizontalAlignment = Element.ALIGN_CENTER, TWO_DIGITS, p.AG.Modifier),
                (Player p) => new("PA", ValueToPhrase(p.PA), (c, _) => c.HorizontalAlignment = Element.ALIGN_CENTER, TWO_DIGITS, p.PA.Modifier),
                (Player p) => new("AR", ValueToPhrase(p.AV), (c, _) => c.HorizontalAlignment = Element.ALIGN_CENTER, TWO_DIGITS, p.AV.Modifier),
                (Player p) => new("BP", p.Nigglings.Val > 0 ? ValueToPhrase(p.Nigglings) : CreatePhrase("-"), (c, isHeader) => c.HorizontalAlignment = Element.ALIGN_CENTER, TWO_DIGITS, p.Nigglings.Modifier),
                (Player p) => {
                    var result = new ColumnInfo("Compétences");

                    var normalSkills = p.Skills.Where(s => !s.IsExtra).ToList();
                    var extraSkills = p.Skills.Where(s => s.IsExtra).ToList();
                    Chunk normal = normalSkills.Count > 0 ? new Chunk(string.Join(", ", normalSkills.Select(s => s.Name))) { Font = RegularFont } : null;

                    // single player for this position, or player without any base skill => we put all the skills in the same cell
                    if (p.Position.Val != null && (normalSkills.Count == 0 || team.Players.Count(pp => pp.Position.Val == p.Position.Val) == 1))
                    {
                        var extra = extraSkills.Count > 0 ? new Chunk(string.Join(", ", extraSkills.Select(s => s.Name))) { Font = BoldFont } : null;
                        var chunks = (new Chunk[] { normal, extra}).Where(c => c != null).ToList();
                        if (chunks.Count == 2)
                        {
                            chunks.Insert(1, new Chunk(", "));
                        }
                        chunks.Add(new Chunk(" " + new string('\xA0', 10))); // for an extra competence
                        result.Value = CreatePhrase([.. chunks]);
                        result.Callback = (c, isHeader) => c.Colspan = 2;
                        return result;
                    }

                    // more than one player sharing the same position
                    result.Callback = (c, isHeader) =>
                        {
                            if (isHeader) c.Colspan = 2;
                            else c.Rowspan = team.Players.Select(pp => pp.Position.Val).Count(pp => pp == p.Position.Val);
                        };

                    if (p.Position.Val != null && p.Position.Val == lastPosition)
                    {
                        return result;
                    }

                    lastPosition = p.Position.Val;
                    result.Value = CreatePhrase(normal);
                    return result;
                },
                (Player p) => {
                    var result = new ColumnInfo();

                    var normalSkills = p.Skills.Where(s => !s.IsExtra).ToList();
                    if (p.Position.Val != null && (normalSkills.Count == 0 || team.Players.Count(pp => pp.Position.Val == p.Position.Val) == 1))
                    {
                        return result;
                    }

                    var extraSkills = p.Skills.Where(s => s.IsExtra).ToList();
                    var extra = extraSkills.Count > 0 ? new Chunk(string.Join(", ", extraSkills.Select(s => s.Name))) { Font = BoldFont } : null;
                    return new ColumnInfo(null, CreatePhrase(extra, new Chunk(" " + new string('\xA0', 10))));
                },
                (Player p) => new("N°", ValueToPhrase(p.Number, BoldFont), (c, _) => { c.BackgroundColor = LIGHTER_GRAY; c.HorizontalAlignment = Element.ALIGN_CENTER; c.BorderWidthLeft = c.BorderWidthRight = 2; }, TWO_DIGITS),
                (Player p) => new("XP dép", CreatePhrase($"{p.ExperienceSpent.Val}"), (c, _) => c.HorizontalAlignment = Element.ALIGN_CENTER, FOUR_DIGITS),
                (Player p) => new("XP rest", CreatePhrase($"{p.Experience.Val - p.ExperienceSpent.Val}"), (c, _) => c.HorizontalAlignment = Element.ALIGN_CENTER, TWO_DIGITS),
                (Player p) => new("Bles.", CreatePhrase(), (c, _) => c.HorizontalAlignment = Element.ALIGN_CENTER, SIX_DIGITS),
                (Player p) => new("TD", CreatePhrase(), (c, isHeader) => ApplyCircledStyle(c, isHeader, ValueToPhrase(p.Touchdown, RegularFontGray)), TWO_DIGITS),
                (Player p) => new("Réu", CreatePhrase(), (c, isHeader) => ApplyCircledStyle(c, isHeader , ValueToPhrase(p.Completion, RegularFontGray)), TWO_DIGITS),
                (Player p) => new("Sor", CreatePhrase() , (c, isHeader) =>  ApplyCircledStyle(c, isHeader , ValueToPhrase(p.Casualty, RegularFontGray)), TWO_DIGITS),
                (Player p) => new("Dév", CreatePhrase() , (c, isHeader) => ApplyCircledStyle(c, isHeader , ValueToPhrase(p.Deviation, RegularFontGray)), TWO_DIGITS),
                (Player p) => new("Int", CreatePhrase() , (c, isHeader) => ApplyCircledStyle(c, isHeader , ValueToPhrase(p.Interception, RegularFontGray)), TWO_DIGITS),
                (Player p) => new("JPV", CreatePhrase(), (c, isHeader) => ApplyCircledStyle(c, isHeader , ValueToPhrase(p.MVP, RegularFontGray)), TWO_DIGITS),
                (Player p) => new("Viré", CreatePhrase(), (c, isHeader) => c.HorizontalAlignment = Element.ALIGN_CENTER, TWO_DIGITS),
            };

            void ApplyCircledStyle(PdfPCell cell, bool isHeader, Phrase phrase)
            {
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.CellEvent = isHeader ? null : new DiagonalCellEvent(phrase);
            }

            table = new PdfPTable(dataCells.Length)
            {
                SpacingBefore = 20f,
                SpacingAfter = 00f,
                LockedWidth = true,
                TotalWidth = 800f,
            };

            var maxWidths = new float[dataCells.Length];

            PdfPCell GetTitleCell(string text, CellRenderCallback callback)
            {
                var chunk = new Chunk(text) { Font = BoldFont };
                var cell = CreateCell(CreatePhrase(chunk)).WithBorder();
                callback?.Invoke(cell, true);
                SetCellStyle(cell);
                return cell;
            }

            int i = 0;
            foreach (var dataCell in dataCells)
            {
                (string title, _, CellRenderCallback callback, string template, _) = dataCell.Invoke(new Player());
                PdfPCell cell = GetTitleCell(title, callback);
                if (title != null)
                {
                    table.AddCell(cell);
                }
                maxWidths[i] = template == null ? -1 : GetWidth(cell);
                if (maxWidths[i] >= 0) maxWidths[i] = Math.Max(maxWidths[i], GetWidth(cell));
                ++i;
            }

            var effectiveWidthsByRow = new List<float[]>();
            var cellRows = new List<PdfPCell[]>();

            foreach (var player in team.Players)
            {
                i = 0;
                var widths = new float[dataCells.Length];
                var cells = new PdfPCell[dataCells.Length];
                effectiveWidthsByRow.Add(widths);
                cellRows.Add(cells);
                foreach (var dataCell in dataCells)
                {
                    (_, Phrase value, CellRenderCallback callback, _, Modifier modifier) = dataCell.Invoke(player);
                    var phrase = value;

                    if (phrase != null)
                    {
                        var cell = CreateCell(phrase).WithBorder();
                        callback?.Invoke(cell, false);
                        SetCellStyle(cell);

                        if (modifier == Modifier.Malus)
                        {
                            cell.BackgroundColor = BOLD_RED;
                        }
                        else if (modifier == Modifier.Bonus)
                        {
                            cell.BackgroundColor = BOLD_GREEN;
                        }


                        table.AddCell(cell);
                        widths[i] = GetWidth(cell);
                        cells[i] = cell;
                        if (maxWidths[i] >= 0) maxWidths[i] = Math.Max(maxWidths[i], widths[i]);
                    }
                    else
                    {
                        maxWidths[i] = -1;
                    }
                    ++i;
                }
            }

            // we want to balance name, position, and skills so that there are the fewer lines possible

            var fixedWidths = maxWidths.Where(w => w != -1).Sum();
            var remainingWidth = table.TotalWidth - fixedWidths;

            var freeWidthIndices = maxWidths.Select((w, index) => (w, index)).Where(p => p.w == -1).Select(p => p.index).ToList();

            var minNumberOfLines = int.MaxValue;

            var finalFreeWidths = freeWidthIndices.Select(_ => 0f).ToArray();
            var testw = finalFreeWidths.Select(_ => 0f).ToArray();

            var freeColCount = testw.Length;

            var minWidths = freeWidthIndices.Select(fwi => cellRows.Max(t => GetMinWidth(t[fwi]))).ToArray();
            var minPercentages = minWidths.Select(mw => (int)Math.Ceiling(100 * mw / remainingWidth)).Take(minWidths.Length - 1).ToArray();
            var values = GetPercent(freeColCount, minPercentages);
            foreach (var v in values)
            {
                var sum = 0f;
                for (int k = 0; k < freeColCount; ++k)
                {
                    testw[k] = remainingWidth * v[k] / 100f;
                    if (k != freeColCount - 1) sum += testw[k];
                }
                testw[freeColCount - 1] = remainingWidth - sum;

                var numberOfLines = cellRows
                    .Select(cells => GetMaxNumberOfLines(cells, testw, freeWidthIndices))
                    .Sum();
                if (numberOfLines < minNumberOfLines || (numberOfLines == minNumberOfLines && finalFreeWidths.Last() < testw.Last()))
                {
                    Array.Copy(testw, 0, finalFreeWidths, 0, freeColCount);
                    minNumberOfLines = numberOfLines;
                }
            }

            for (int k = 0; k < freeColCount; ++k)
            {
                maxWidths[freeWidthIndices[k]] = finalFreeWidths[k];
            }

            table.SetWidths(maxWidths.Select(w => w == -1 ? remainingWidth : w).ToArray());

            doc.Add(table);

            table = new PdfPTable(dataCells.Length)
            {
                SpacingBefore = 00f,
                SpacingAfter = 00f,
                LockedWidth = true,
                TotalWidth = 800f,

            };
            var playerCountCell = CreateCell(CreatePhrase(team.Players.Count.ToString() + " joueurs", BoldFont));
            SetCellStyle(playerCountCell);
            playerCountCell.Colspan = 2;
            table.AddCell(playerCountCell);
            for (int j = 2; j < dataCells.Length; ++j) table.AddCell(CreateCell(CreatePhrase("")));
            table.SetWidths(maxWidths.Select(w => w == -1 ? remainingWidth : w).ToArray());
            doc.Add(table);

            // tables from the lower part
            var anyLeader = team.Players.Any(p => p.Skills.Any(s => s.Name == "Chef"));

            var leftLines = new[] {
                (CreatePhrase("Valeur", BoldFont), CreatePhrase($"{team.TeamValue.Val:n0}")),
                (CreatePhrase("Trésor", BoldFont), CreatePhrase($"{team.Treasury.Val:n0}")),
                (CreatePhrase("Apothicaire", BoldFont), team.Apothicary.Val ? CreatePhrase(CreateChunk("Oui 🎲"), CreateChunk("✙", RegularFontRed)) : CreatePhrase("Non")),
                (CreatePhrase("Relances", BoldFont),
                CreatePhrase(
                    ValueToChunk(team.Rerolls),
                    anyLeader ? CreateChunk(" + chef (") : CreateChunk(" ("),
                    CreateChunk(string.Concat(Enumerable.Range(0, team.Rerolls.Val).Select(_ => "🎲")), EmojiFont),
                    anyLeader ?CreateChunk(" + ") : null,
                    anyLeader ? CreateChunk( "🎲", EmojiFont) : null,
                    CreateChunk(")"))),
                (CreatePhrase("Assistants", BoldFont), ValueToPhrase(team.Assistants)),
                (CreatePhrase("Pom-pom girls", BoldFont), ValueToPhrase(team.CheerLeaders)),
                (CreatePhrase("Fans dévoués", BoldFont), ValueToPhrase(team.Popularity)),
                (CreatePhrase("Coût relance", BoldFont), CreatePhrase($"{team.RerollCost.Val:n0}")),
            };

            var tickBoxChunk = CreateChunk("☐", UnicodeFont);
            var meteos = new[] { "2 - Canicule", "3 - Très ensoleillé", "4-10 - Clément", "11 - Averse", "12 - Blizzard" };
            var middleLines = new[]
            {
                //(CreatePhrase("Météo", BoldFont), CreatePhrase(meteos.SelectMany(m => new [] { tickBoxChunk, CreateChunk($" {m}     ") }).ToArray()), null, null),
                (CreatePhrase("Aggressions", BoldFont), CreatePhrase(""),CreatePhrase("Sorties du public", BoldFont), CreatePhrase("")),
                (CreatePhrase("Avant-match", BoldFont), null, null, null),
                (CreatePhrase("Fans (1D3 + Fans dévoués)", BoldFont), CreatePhrase(""), CreatePhrase("Météo", BoldFont), CreatePhrase("")),
                (CreatePhrase("Petite monnaie", BoldFont), CreatePhrase(diff < 0 ? $"{-diff/1000}K" : "0"), CreatePhrase($"Trésor dépensé", BoldFont), CreatePhrase("")),
                (CreatePhrase("Coups de pouce", BoldFont), CreatePhrase($""), CreatePhrase("Prière(s) à Nuffle", BoldFont), CreatePhrase("")),
                (CreatePhrase("Après-match", BoldFont), null, null, null),
                (CreatePhrase("Gains (Fans / 2 + TD)", BoldFont), CreatePhrase(""), null, null),
                (CreatePhrase("Achats", BoldFont), CreatePhrase(""), null, null),
                (CreatePhrase("Fans dévoués", BoldFont), CreatePhrase("+1             =             -1"), CreatePhrase("Erreurs coûteuses", BoldFont), CreatePhrase("D6 :             D3 :           2D6 :")),
            };

            var leftTableColumnsCount = leftLines.First().GetType().GetGenericArguments().Length;
            var middleTableColumnsCount = middleLines.First().GetType().GetGenericArguments().Length;
            var rightTableColumnsCount = 7;

            var rows = Math.Max(Math.Max(leftLines.Length, middleLines.Length), 12);
            var tableGrid = new PdfPCell[leftTableColumnsCount + 1 + middleTableColumnsCount + 1 + rightTableColumnsCount, rows];

            // bottom left table
            int x = 0, y = 0;
            foreach (var line in leftLines)
            {
                FillConsecutive(tableGrid, x, y,
                    CreateCell(line.Item1).ToRight(),
                    CreateCell(line.Item2));
                y++;
            }

            // bottom middle table
            x = 0; y = 0;
            int offset = 3;
            foreach (var line in middleLines)
            {
                var cell0 = CreateCell(line.Item1).ToRight();
                if (line.Item2 == null)
                {
                    cell0.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell0.Colspan = 4;
                    cell0.BackgroundColor = LIGHTER_GRAY;
                }
                var cell1 = line.Item2 != null ? CreateCell(line.Item2).WithColspan(line.Item3 == null ? 3 : 1) : null;
                if (cell1 != null && line.Item2.Content == "") { cell1.CellEvent = new InnerBorderCellEvent(); }
                var cell2 = line.Item3 != null ? CreateCell(line.Item3).ToRight() : null;
                var cell3 = line.Item4 != null ? CreateCell(line.Item4) : null;
                if (cell3 != null && line.Item4.Content == "") { cell3.CellEvent = new InnerBorderCellEvent(); }

                FillConsecutive(tableGrid, x + offset, y, cell0, cell1, cell2, cell3);
                y++;
            }
            tableGrid[offset + 1, 0].HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;

            // bottom right table
            offset = 8;
            var expensiveMistakesHeaderCell = CreateCell(CreatePhrase("Erreurs coûteuses (1D6)", BoldFont)).ToCenter().WithColspan(6);
            tableGrid[offset + 1, 0] = expensiveMistakesHeaderCell;
            foreach (var item in new[] { "Trésor", "1", "2", "3", "4", "5", "6" })
            {
                var cell = CreateCell(CreatePhrase(item, BoldFont)).ToCenter();
                tableGrid[x + offset, 1] = cell;
                x++;
            }

            var expensiveMistakes = new[]
            {
                (100, 195, new List<(Severity severity,int len)> { (Severity.MinorIncident, 1), (Severity.CrisisAverted, 5) }),
                (200, 295, new List<(Severity severity,int len)> { (Severity.MinorIncident, 2), (Severity.CrisisAverted, 4) }),
                (300, 395, new List<(Severity severity,int len)> { (Severity.MajorIncident, 1), (Severity.MinorIncident, 2), (Severity.CrisisAverted, 3) }),
                (400, 495, new List<(Severity severity,int len)> { (Severity.MajorIncident, 2), (Severity.MinorIncident, 2), (Severity.CrisisAverted, 2) }),
                (500, 595, new List<(Severity severity,int len)> { (Severity.Catastrophe, 1),(Severity.MajorIncident, 2), (Severity.MinorIncident, 2), (Severity.CrisisAverted, 1) }),
                (600, -1, new List<(Severity severity,int len)> { (Severity.Catastrophe, 2),(Severity.MajorIncident, 2), (Severity.MinorIncident, 2) }),
            };
            y = 2;

            foreach (var expensiveMistake in expensiveMistakes)
            {
                var lower = expensiveMistake.Item1;
                var upper = expensiveMistake.Item2;
                var cols = expensiveMistake.Item3;

                var cell = CreateCell(upper >= 0 ? CreatePhrase($"{lower}K → {upper}K") : CreatePhrase($"{lower}K et +")).ToCenter();
                tableGrid[offset, y] = cell;
                x = 0;
                var backColors = new[] { LIGHTER_GRAY, BaseColor.LIGHT_GRAY, BaseColor.GRAY };
                var fonts = new[] { EmojiFont, EmojiFont, EmojiFont, EmojiFontWhite };
                var symbols = new[] { "☁", "⚡", "⚡⚡", "💀" };
                foreach (var span in cols)
                {
                    (Severity severity, int len) = span;
                    var text = symbols[(int)severity];
                    cell = CreateCell(CreatePhrase(text, fonts[(int)severity])).ToCenter().WithColspan(len);
                    if ((int)severity > 0) cell.BackgroundColor = backColors[(int)severity - 1];
                    tableGrid[offset + 1 + x, y] = cell;
                    x += cell.Colspan;
                }
                y++;
            }
            var expensiveMistakeDescriptions = new[]
            {
                ("☁", "Crise évitée"),
                ("⚡", "Incident mineur (perte de 1D3 x 10K)"),
                ("⚡⚡", "Incident majeur (perte de la moitié du trésor, arrondie aux 5K inférieurs)"),
                ("💀", "Catastrophe (perte de tout le trésor, sauf 2D6 x 10K)"),
            };
            foreach (var desc in expensiveMistakeDescriptions)
            {
                tableGrid[offset, y] = CreateCell(CreatePhrase(desc.Item1, EmojiFont)).ToRight();
                tableGrid[offset + 1, y] = CreateCell(CreatePhrase(desc.Item2, SmallFont)).WithColspan(6);
                y++;
            }

            table = new PdfPTable(tableGrid.GetUpperBound(0) + 1)
            {
                SpacingBefore = 00f,
                SpacingAfter = 00f,
                LockedWidth = true,
                TotalWidth = 842 - 24 * 2 // page width minus margins,
            };

            maxWidths = new float[tableGrid.GetUpperBound(0) + 1];

            var spaceBetweenColumnsWidth = 10;
            var spaceBetweenColumnsIndices = new[] { 2, 7 };

            for (y = 0; y <= tableGrid.GetUpperBound(1); ++y)
            {
                for (x = 0; x <= tableGrid.GetUpperBound(0); ++x)
                {
                    var cell = tableGrid[x, y] ?? CreateCell(CreatePhrase(""));

                    SetCellStyle(cell);
                    cell.MinimumHeight = 19;

                    if (cell.Colspan == 1)
                    {
                        maxWidths[x] = Math.Max(maxWidths[x], spaceBetweenColumnsIndices.Contains(x) ? spaceBetweenColumnsWidth : GetWidth(cell));
                    }
                    else
                    {
                        x += cell.Colspan - 1;
                    }
                    table.AddCell(cell);
                }
            }

            var digitMaxWidth = maxWidths.Reverse().Take(6).Max();
            for (int d = 1; d <= 6; ++d)
            {
                maxWidths[maxWidths.Length - d] = digitMaxWidth;
            }

            var freeColumnIndices = new[] { 4, 6 };
            fixedWidths = maxWidths.Sum() - freeColumnIndices.Sum(index => maxWidths[index]);
            remainingWidth = table.TotalWidth - fixedWidths;

            table.SetWidths(maxWidths.Select((w, index) => freeColumnIndices.Contains(index) ? remainingWidth / freeColumnIndices.Length : w).ToArray());

            table.HorizontalAlignment = Element.ALIGN_LEFT;
            doc.Add(table);
        }

        private IEnumerable<int[]> GetPercent(int length, int[] minPercentages)
        {
            var result = new int[length];
            return GetPercent(0, minPercentages, 100 - length + 1, result);
        }

        private IEnumerable<int[]> GetPercent(int index, int[] minPercentages, int max, int[] result)
        {
            if (index == result.Length - 1)
            {
                result[index] = max + index;
                yield return result;
            }
            else
            {
                for (int i = minPercentages[index]; i < max; ++i)
                {
                    result[index] = i;
                    foreach (var r in GetPercent(index + 1, minPercentages, max - i, result))
                        yield return r;
                }
            }
        }

        private void FillConsecutive(PdfPCell[,] tableGrid, int x, int y, params PdfPCell[] cells)
        {
            foreach (var cell in cells)
            {
                if (cell != null)
                {
                    tableGrid[x, y] = cell;
                }
                ++x;
            }
        }

        private int GetMaxNumberOfLines(PdfPCell[] cells, float[] widths, List<int> freeWidthIndices)
        {
            var result = 0;

            for (int i = 0; i < freeWidthIndices.Count; ++i)
            {
                var index = freeWidthIndices[i];
                var cell = cells[index];
                var width = widths[i];
                if (cell == null)
                {
                    result = Math.Max(result, 1);
                    continue;
                }

                if (cell.Colspan == 2 && (i + 1) < freeWidthIndices.Count && freeWidthIndices[i + 1] == index + 1)
                {
                    // consecutive cells that form a span have a width equal to the width of the consecutive cells
                    width += widths[i + 1];
                }
                result = Math.Max(result, GetNumberOfLines(cell, width));
            }
            return result;
        }

        private int GetNumberOfLines(PdfPCell cell, float width)
        {
            if (cell == null) return 1;
            width -= (cell.PaddingLeft + cell.PaddingRight + cell.BorderWidthLeft + cell.BorderWidthRight);
            int result = 1;
            var chunks = cell.Phrase?.Cast<Chunk>().ToList();
            var x = 0f;
            foreach (Chunk chunk in chunks)
            {
                float chunkSpaceWidth = GetTextWidth(chunk, " ");
                foreach (var word in chunk.ToString().Split(' '))
                {
                    var wordWidth = GetTextWidth(chunk, word);
                retry:
                    if (x + wordWidth > width)
                    {
                        if (x == 0)
                        {
                            return 100; // the word doesn't fit in the space, return an arbitrarily high value
                        }
                        result++;
                        x = 0;
                        goto retry;
                    }

                    x += wordWidth;
                    x += chunkSpaceWidth;
                }
            }
            if (cell.Rowspan > 1)
            {
                return Math.Max(1, (int)Math.Ceiling(result / (double)cell.Rowspan));
            }
            return result;
        }

        private Dictionary<string, float> TextWidthByChunk { get; } = [];

        private float GetTextWidth(Chunk chunk, string text)
        {
            var key = chunk.Font.Familyname + chunk.Font.CalculatedSize + text;
            if (TextWidthByChunk.TryGetValue(key, out float result))
            {
                return result;
            }
            result = chunk.Font
                .GetCalculatedBaseFont(specialEncoding: true)
                .GetWidthPoint(text, chunk.Font.CalculatedSize) * chunk.HorizontalScaling;
            TextWidthByChunk.Add(key, result);
            return result;
        }

        const float EMPIRICAL_FACTOR = 1.05f; // because something's wrong in the computation of the cell content size

        private float GetMinWidth(PdfPCell cell)
        {
            if (cell == null) return 0;
            var chunks = cell.Phrase?.Cast<Chunk>().ToList();
            var minWidth = chunks == null || chunks.Count == 0 ? 0 : chunks
                .Select(c => c.ToString().Split(' ').Min(t => GetTextWidth(c, t)))
                .Min();
            return EMPIRICAL_FACTOR * (minWidth + cell.PaddingLeft + cell.PaddingRight + cell.BorderWidthLeft + cell.BorderWidthRight);
        }

        private float GetWidth(PdfPCell cell)
        {
            if (cell == null) return 0;
            var chunks = cell.Phrase?.Cast<Chunk>().ToList();
            var width = (chunks == null || chunks.Count == 0 ? 0 : chunks.Sum(chunk => chunk.GetWidthPoint()));
            return EMPIRICAL_FACTOR * (width + cell.PaddingLeft + cell.PaddingRight + cell.BorderWidthLeft + cell.BorderWidthRight + cell.BorderWidth);
        }

        private void SetCellStyle(PdfPCell cell)
        {
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.UseAscender = true;
            cell.UseDescender = true;
            cell.Padding = 0;
            cell.PaddingTop = 2;
            cell.PaddingBottom = 2;
            if (cell.HorizontalAlignment == Element.ALIGN_LEFT || cell.HorizontalAlignment == Element.ALIGN_CENTER)
            {
                cell.PaddingLeft = 4;
            }
            if (cell.HorizontalAlignment == Element.ALIGN_RIGHT || cell.HorizontalAlignment == Element.ALIGN_CENTER)
            {
                cell.PaddingRight = 4;
            }
        }

        private delegate void CellRenderCallback(PdfPCell cell, bool isHeader);

        private class ColumnInfo(string title = null, Phrase value = null, CellRenderCallback callback = null, string template = null, Modifier modifier = Modifier.None)
        {
            public string Title { get; set; } = title;
            public Phrase Value { get; set; } = value;
            public CellRenderCallback Callback { get; set; } = callback;
            public string Template { get; set; } = template;
            public Modifier Modifier { get; set; } = modifier;

            internal void Deconstruct(out string title, out Phrase value, out CellRenderCallback callback, out string template, out Modifier modifier)
            {
                title = Title;
                value = Value;
                callback = Callback;
                template = Template;
                modifier = Modifier;
            }
        }
    }
}
