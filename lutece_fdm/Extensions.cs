﻿using iTextSharp.text;
using iTextSharp.text.pdf;

namespace lutece_fdm
{
    public static class Extensions
    {
        public static PdfPCell ToLeft(this PdfPCell cell)
        {
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            return cell;
        }

        public static PdfPCell ToCenter(this PdfPCell cell)
        {
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            return cell;
        }

        public static PdfPCell ToRight(this PdfPCell cell)
        {
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            return cell;
        }

        public static PdfPCell WithBorder(this PdfPCell cell, float borderWidth = 0.5f)
        {
            cell.Border = Rectangle.BOX;
            cell.BorderWidth = borderWidth;
            return cell;
        }

        public static PdfPCell WithColspan(this PdfPCell cell, int colspan)
        {
            cell.Colspan = colspan;
            return cell;
        }

        public static PdfPCell WithBackgroundColor(this PdfPCell cell, BaseColor color)
        {
            cell.BackgroundColor = color;
            return cell;
        }
    }
}
