﻿namespace lutece_fdm
{
    public class Skill
    {
        public string Name { get; set; }
        public bool IsExtra { get; set; }

        public Skill(string name, bool isExtra)
        {
            name = name.Replace('\xa0', ' ');
            name = name.Replace("Chataîgne", "Châtaigne");
            name = name.Replace("Regeneration", "Régénération");
            name = name.Replace("Perso", "Solitaire");
            name = name.Replace("Prise Sure", "Prise Sûre");
            name = name.Replace("Crane épais", "Crâne épais");
            name = name.Replace("(", " (");
            Name = name;
            IsExtra = isExtra;
        }
    }
}
