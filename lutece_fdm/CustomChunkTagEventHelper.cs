﻿using iTextSharp.text;
using iTextSharp.text.pdf;

using System;

namespace lutece_fdm
{
    public class CustomChunkTagEventHelper : PdfPageEventHelper
    {
        public const string CIRCLE = "circle";

        public override void OnGenericTag(
          PdfWriter writer, Document pdfDocument, Rectangle rect, string text)
        {
            PdfContentByte content = writer.DirectContentUnder;
            content.SaveState();

            if (CIRCLE.Equals(text))
            {
                content.SetCMYKColorStroke(10, 10, 10, 60);
                var max = Math.Max(rect.Width, rect.Height) / 2;
                content.Circle((rect.Left + rect.Right) / 2, (rect.Top + rect.Bottom) / 2 - 1f, max);
                content.Stroke();
            }

            content.RestoreState();
        }
    }
}
