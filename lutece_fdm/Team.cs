﻿using System.Collections.Generic;

namespace lutece_fdm
{
    public class Team
    {
        public Value<string> TeamName { get; set; }
        public Value<string> CoachName { get; set; }
        public Value<string> Race { get; set; }
        public List<Player> Players { get; } = new List<Player>();
        public Value<bool> Apothicary { get; set; }
        public Value<int> TeamValue { get; set; }
        public Value<int> RerollCost { get; set; }
        public Value<int> Assistants { get; set; }
        public Value<int> CheerLeaders { get; set; }
        public Value<int> Popularity { get; set; }
        public Value<int> Rerolls { get; set; }
        public Value<int> Treasury { get; set; }
    }
}
