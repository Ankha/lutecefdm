﻿using System;
using System.Globalization;
using System.Net;

namespace lutece_fdm
{
    class Program
    {
        static int Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Syntaxe:");
                Console.WriteLine("lutece_fdm \"<url>\"");
                Console.WriteLine("");
                Console.WriteLine("url: l'adresse de la feuille de match, par exemple http://www.lutececup.org/roster.php?match_id=153&team_id=32");
                Console.WriteLine("     attention à bien l'encadrer de guillemets");
                return 1;
            }

            var url = args[0];

            if (!UrlValidator.IsUrlValid(url))
            {
                Console.WriteLine("L'url a l'air suspecte, elle devrait ressembler à : http://www.lutececup.org/roster.php?match_id=...&team_id=...");
                Console.Write("Continuer ? (O/n) ");
                string r = ((char)Console.Read()).ToString();
                do
                {
                    if (string.Compare(r, "n", true) == 0)
                    {
                        return 0;
                    }
                    if (r == "" || string.Compare(r, "o", true) == 0)
                    {
                        break;
                    }
                    r = ((char)Console.Read()).ToString();
                }
                while (true);
            }

            var frFr = new CultureInfo("fr-FR");
            CultureInfo.CurrentUICulture = frFr;
            CultureInfo.CurrentCulture = frFr;

            string htmlCode;
            using (WebClient client = new WebClient())
            {
                Console.WriteLine("Téléchargement de " + url);
                htmlCode = client.DownloadString(url);
            }

            var teamParser = new TeamParser();
            var teams = teamParser.CollectTeams(htmlCode);
            Console.WriteLine($"{teams[0].TeamName.Val} vs {teams[1].TeamName.Val}");

            string outputPath = $"Lutece - {teams[0].TeamName.Val} vs {teams[1].TeamName.Val}.pdf";

            var generator = new PdfGenerator();
            generator.Create(teams, outputPath);

            return 0;
        }
    }
}
