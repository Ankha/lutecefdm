﻿using System.Collections.Generic;

namespace lutece_fdm
{
    public class Player
    {
        public Value<string> Number { get; set; }
        public Value<string> Position { get; set; }
        public Value<string> Name { get; set; }
        public Value<int> MA { get; set; }
        public Value<int> ST { get; set; }
        public Value<string> AG { get; set; }
        public Value<string> PA { get; set; }
        public Value<string> AV { get; set; }
        public List<Skill> Skills { get; set; } = [];
        public Value<int> Nigglings { get; set; }
        public Value<int> Experience { get; set; }
        public Value<int> ExperienceSpent { get; set; }
        public Value<int> Touchdown { get; set; }
        public Value<int> Completion { get; set; }
        public Value<int> Casualty { get; set; }
        public Value<int> Deviation { get; set; }
        public Value<int> Interception { get; set; }
        public Value<int> MVP { get; set; }
    }
}