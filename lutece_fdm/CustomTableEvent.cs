﻿using iTextSharp.text;
using iTextSharp.text.pdf;

namespace lutece_fdm
{
    public class InnerBorderCellEvent : IPdfPCellEvent
    {
        public void CellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
        {
            float x1 = position.Left + 1;
            float x2 = position.Right - 1;
            float y1 = position.Top - 1;
            float y2 = position.Bottom + 1;

            var canvas = canvases[PdfPTable.LINECANVAS];
            canvas.Rectangle(x1, y1, x2 - x1, y2 - y1);
            canvas.SetLineWidth(0.5f);
            canvas.Stroke();
            canvas.ResetRGBColorStroke();
        }
    }


    public class DiagonalCellEvent : IPdfPCellEvent
    {
        public DiagonalCellEvent(Phrase phrase)
        {
            Phrase = phrase;
        }

        public Phrase Phrase { get; }

        public void CellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
        {
            PdfContentByte canvas = canvases[PdfPTable.TEXTCANVAS];
            canvas.SetCMYKColorFill(10, 10, 10, 60);
            ColumnText.ShowTextAligned(canvas, Element.ALIGN_RIGHT, Phrase, position.GetRight(2), position.GetBottom(2), 0);

            //canvas = canvases[PdfPTable.LINECANVAS];
            //canvas.SetCMYKColorStroke(10, 10, 10, 60);
            //canvas.MoveTo(position.Right, position.Top);
            //canvas.LineTo(position.Left, position.Bottom);
            //canvas.Stroke();
            cell.Phrase = null;
        }
    }
}
