﻿using System.Text.RegularExpressions;

namespace lutece_fdm
{
    public static class UrlValidator
    {
        public static bool IsUrlValid(string url)
        {
            var regex = new Regex(@"^https:\/\/(www\.)?lutececup\.org\/roster\.php\?match_id=[0-9]+&team_id=[0-9]+$");
            return regex.IsMatch(url);
        }
    }
}
