﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace lutece_fdm
{
    public class TeamParser
    {
        public static Regex DoubleIntRegex = new("(\\d+) \\((\\d+)\\)", RegexOptions.Compiled);

        public List<Team> CollectTeams(string htmlCode)
        {
            var teams = new List<Team>();
            var lines = htmlCode.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            var lineIndex = 0;
            teams.Add(CollectTeam(lines, "team_home", ref lineIndex));
            teams.Add(CollectTeam(lines, "team_visit", ref lineIndex));
            return teams;
        }

        private Team CollectTeam(string[] lines, string id, ref int lineIndex)
        {
            var result = new Team();
            MoveUntil(lines, ref lineIndex, id, "<h1>");
            result.TeamName = GetString(lines[lineIndex], "h1");

            MoveUntil(lines, ref lineIndex, "Coach :");
            result.CoachName = GetString(lines[lineIndex], "/span");

            MoveUntil(lines, ref lineIndex, "Race :");
            result.Race = GetString(lines[lineIndex], "/span");

            MoveUntil(lines, ref lineIndex, "Race :");
            result.Race = GetString(lines[lineIndex], "/span");

            MoveUntil(lines, ref lineIndex, "compo_team", "<tbody>");

            var firstIndex = lineIndex;
            MoveUntil(lines, ref lineIndex, "</tbody>");
            var lastIndex = lineIndex;

            lineIndex = firstIndex;

            do
            {
                var player = new Player();
                MoveUntil(lines, ref lineIndex, "<tr>");
                MoveUntil(lines, ref lineIndex, "<td>");
                if (lineIndex >= lastIndex)
                {
                    lineIndex = lastIndex;
                    break;
                }

                player.Number = GetString(lines[lineIndex], "td");

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td>");
                var posName = GetString(lines[lineIndex], "<td>").Val;
                var position = posName.Substring(0, posName.IndexOf(" - <span"));
                if (player.Number.Val == "##")
                {
                    position += " (J)"; // journalier
                }
                player.Position = new Value<string>(position);

                var name = Substring(posName, posName.IndexOf(">") + 1, posName.LastIndexOf("</span>"));
                if (string.IsNullOrEmpty(name)) name = "-";
                player.Name = new Value<string>(name);

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td");
                player.MA = GetInt(lines[lineIndex], "td");

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td");
                player.ST = GetInt(lines[lineIndex], "td");

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td");
                player.AG = GetIntPlus(lines[lineIndex], "td", cannotExceed6: true);

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td");
                player.PA = GetIntPlus(lines[lineIndex], "td", cannotExceed6: true);

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td");
                player.AV = GetIntPlus(lines[lineIndex], "td", cannotExceed6: false);

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td>");
                var skills = GetString(lines[lineIndex], "td").Val;
                var natural = Substring(skills, 0, skills.IndexOf("<span"));
                var extra = skills.IndexOf(">") >= 0 ? Substring(skills, skills.IndexOf(">") + 1, skills.LastIndexOf("</span>")) : "";
                extra = extra.Replace("<span>", null).Replace("</span>", null);
                player.Skills.AddRange(natural.Split(',').Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => new Skill(s.Trim(), false)));
                player.Skills.AddRange(extra.Split(',').Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => new Skill(s.Trim(), true)));

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td");
                player.Nigglings = GetInt(lines[lineIndex], "td");
                if (player.Nigglings.Val > 0) player.Nigglings = new Value<int>(player.Nigglings.Val, false, Modifier.Malus);

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td class=\"bold\">");
                (player.Experience, player.ExperienceSpent) = GetDoubleInt(lines[lineIndex], "td");

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td>");
                player.Touchdown = GetInt(lines[lineIndex], "td");

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td>");
                player.Completion = GetInt(lines[lineIndex], "td");

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td>");
                player.Casualty = GetInt(lines[lineIndex], "td");

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td>");
                player.Deviation = GetInt(lines[lineIndex], "td");

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td>");
                player.Interception = GetInt(lines[lineIndex], "td");

                ++lineIndex; MoveUntil(lines, ref lineIndex, "<td>");
                player.MVP = GetInt(lines[lineIndex], "td");

                var index = result.Players.FindLastIndex(p => p.Position.Val == player.Position.Val);
                if (index == -1)
                    result.Players.Add(player);
                else
                    result.Players.Insert(index + 1, player);
            }
            while (lineIndex < lastIndex);

            var maxTd = result.Players.Max(p => p.Touchdown.Val);
            var maxComp = result.Players.Max(p => p.Completion.Val);
            var maxCas = result.Players.Max(p => p.Casualty.Val);
            var maxInt = result.Players.Max(p => p.Interception.Val);
            foreach (var player in result.Players)
            {
                if (maxTd != 0 && player.Touchdown.Val == maxTd) player.Touchdown = new Value<int>(player.Touchdown.Val, true);
                if (maxComp != 0 && player.Completion.Val == maxComp) player.Completion = new Value<int>(player.Completion.Val, true);
                if (maxCas != 0 && player.Casualty.Val == maxCas) player.Casualty = new Value<int>(player.Casualty.Val, true);
                if (maxInt != 0 && player.Interception.Val == maxInt) player.Interception = new Value<int>(player.Interception.Val, true);
            }

            //var star = new Player
            //{
            //    MA = new Value<int>(7),
            //    ST = new Value<int>(3),
            //    AG = new Value<int>(4),
            //    AV = new Value<int>(7),
            //    Name = new Value<string>("Horkon l'écorcheur"),
            //    Position=new Value<string>("Star 210K"),
            //    Number=new Value<string>("99")
            //};
            //star.Skills.Add(new Skill { Name = "Solitaire" });
            //star.Skills.Add(new Skill { Name = "Esquive" });
            //star.Skills.Add(new Skill { Name = "Poursuite" });
            //star.Skills.Add(new Skill { Name = "Saut" });
            //star.Skills.Add(new Skill { Name = "Blocage multiple" });
            //star.Skills.Add(new Skill { Name = "Poignard" });
            //result.Players.Add(star);

            MoveUntil(lines, ref lineIndex, "encadrement", "Apothicaire", "<td>");
            result.Apothicary = GetBool(lines[lineIndex], "td");

            MoveUntil(lines, ref lineIndex, "Assistants", "<td>");
            result.Assistants = GetInt(lines[lineIndex], "td");

            MoveUntil(lines, ref lineIndex, "Pom-pom Girls", "<td>");
            result.CheerLeaders = GetInt(lines[lineIndex], "td");

            MoveUntil(lines, ref lineIndex, "Valeur", "<td>");
            result.TeamValue = GetInt(lines[lineIndex], "td");

            MoveUntil(lines, ref lineIndex, "Fans d", "<td>");
            result.Popularity = GetInt(lines[lineIndex], "td");

            MoveUntil(lines, ref lineIndex, "Relance", "<td>");
            result.Rerolls = GetInt(lines[lineIndex], "td");

            MoveUntil(lines, ref lineIndex, "Relance", "<td>");
            result.RerollCost = GetInt(lines[lineIndex], "td");

            MoveUntil(lines, ref lineIndex, "Tr&eacute;sor", "<td>");
            result.Treasury = GetInt(lines[lineIndex], "td");

            return result;
        }


        private string Substring(string text, int startIndex, int endIndex)
        {
            return endIndex == -1
                ? text.Substring(startIndex)
                : text.Substring(startIndex, endIndex - startIndex);
        }

        private Value<string> GetString(string line, string tag)
        {
            var startIndex = line.IndexOf("<" + tag) + 1 + tag.Length;
            var currentIndex = startIndex;
            bool isBold = false;
            while (line[currentIndex++] != '>') isBold = true;

            var css = Substring(line, startIndex, currentIndex);

            Modifier modifier = Modifier.None;
            if (css.Contains("bold red")) modifier = Modifier.Malus;
            else if (css.Contains("bold green")) modifier = Modifier.Bonus;

            var endIndex = line.IndexOf("</" + tag);
            return new Value<string>(Substring(line, currentIndex, endIndex), isBold, modifier);

            //var tagSpace = tag.IndexOf(' ');
            //var indexStart = line.IndexOf(tag) + tag.Length;
            //var indexEnd = tagSpace == -1
            //    ? line.IndexOf("</" + tag.Substring(1))
            //    : line.IndexOf("</" + Substring(tag, 1, tagSpace));
            //var value = indexEnd != -1
            //    ? Substring(line, indexStart, indexEnd)
            //    : line.Substring(indexStart);
            //return value.Trim();
        }

        private Value<bool> GetBool(string line, string tag)
        {
            var stringValue = GetString(line, tag);
            return new Value<bool>(stringValue.Val == "Oui", stringValue.IsBold);
        }

        private Value<int> GetInt(string line, string tag)
        {
            var stringValue = GetString(line, tag);
            return new Value<int>(int.Parse(stringValue.Val), stringValue.IsBold, stringValue.Modifier);
        }

        private Value<string> GetIntPlus(string line, string tag, bool cannotExceed6)
        {
            var stringValue = GetString(line, tag);
            var value = int.Parse(stringValue.Val.Substring(0, stringValue.Val.Length - 1));
            return cannotExceed6 && value == 7
                ? new Value<string>("-", stringValue.IsBold) // 7+ is used to indicate that the characteristic cannot be used by the player
                : new Value<string>(value + "+", stringValue.IsBold, stringValue.Modifier);
        }


        private (Value<int>, Value<int>) GetDoubleInt(string line, string tag)
        {
            var stringValue = GetString(line, tag);
            var match = DoubleIntRegex.Match(stringValue.Val);
            if (!match.Success) throw new Exception($"Invalid double value: {stringValue.Val}");

            return (new Value<int>(int.Parse(match.Groups[1].Value), stringValue.IsBold), new Value<int>(int.Parse(match.Groups[2].Value), stringValue.IsBold));
        }

        private void MoveUntil(string[] lines, ref int lineIndex, params string[] ids)
        {
            foreach (var id in ids)
                MoveUntilSingle(lines, ref lineIndex, id);
        }
        private bool MoveUntilSingle(string[] lines, ref int lineIndex, string id)
        {
            while (lineIndex < lines.Length && !lines[lineIndex].Contains(id))
                lineIndex++;
            return lineIndex < lines.Length;
        }
    }
}
