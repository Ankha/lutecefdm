﻿namespace lutece_fdm
{
    public enum Severity
    {
        CrisisAverted = 0,
        MinorIncident,
        MajorIncident,
        Catastrophe
    }
}
